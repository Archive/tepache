﻿"""
 setup.py script for installation using distutils
 Copyright (C) 2005 Fernando San Martin Woerner fsmw@gnome.org
"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of Tepache.
#
# Tepache is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Tepache is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from distutils.core import setup
setup(name='tepache',
      version='1.1',
      author="Sandino Flores Moreno/Fernando San Martin Woerner",
      author_email="sflorem@gnome.org, fsmw@gnome.org",
      url="http://primates.ximian.com/~sandino/python-glade/tepache/",
      license="LGPL",
      description=
""" Tepache is a code sketcher for python that uses pygtk and glade.
It could look like other glade codegens, but it is totally different.""",
      py_modules=['SimpleGladeApp'],
      scripts = ['tepache']
      )