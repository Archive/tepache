#!/usr/bin/env python
# -*- coding: UTF8 -*-
# Fernando San Martín Woerner < fsmw@gnome.org >
# This code is covered by GPL License

"""
ProxyActions demos for SimpleGladeApp
GladeConnect class is a wrapper to mix ProxyActions and
SimpleGladeApp
"""

from GladeConnect import GladeConnect
import gtk

class Test(GladeConnect):
    
    def __init__(self):
        GladeConnect.__init__(self, "test.glade")
    
    def run(self):
        gtk.main()

if __name__ == '__main__':
    t = Test()
    t.run()