#!/usr/bin/env python
# -*- coding: utf-8 -*-
# GladeConnect -- Clase base para conectar glade y python

# This file is part of Gestor.
#
# Gestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# Copyright Fernando San Martín Woerner < fsmw@gnome.org >
# Sandino Flores Moreno < sandino@ximian.com >
# Víctor Benitez T. < vbenitez@galilea.cl >

"""
GladeConnect wrapper for SimpleGladeApp, adding ProxyActions
to validate data in entry fields
"""
import pygtk

import gtk.glade
import gtk
import sys
import os
import inspect
import time
from SimpleGladeApp import SimpleGladeApp

class PrefixActions:
    """
    Base class for prefix actions, implements:

    enc: EntryCompletions
    Ent: Entry Fields not empty
    """
    def __init__(self):
        self.mandatories = []

    def prefix_name(self, widget):
        def validate(widget):
            text = widget.get_text()
            self.set_error_status(widget, len(text) < 1 or len(text) > 30)
        def complete(widget, event):
            text = widget.get_text()
            cap = lambda s: s.capitalize()
            tokens = text.split()
            tokens = map(cap, tokens)
            text = " ".join(tokens)
            widget.set_text(text)
        widget.connect("changed", validate)
        widget.connect("focus-out-event", complete)

    def prefix_Name(self, widget):
        self.prefix_name(widget)
        self.add_mandatory(widget)

    def prefix_date(self, widget):
        def parse_date(text):
            (cY,cm,cd) = time.localtime()[0:3]
            try:
                (d,) = time.strptime(text, "%d")[2:3]
                m,Y = cm,cY
            except ValueError:
                try:
                    (m,d) = time.strptime(text, "%d/%m")[1:3]
                    Y = cY
                except:
                    (Y,m,d) = time.strptime(text, "%d/%m/%Y")[0:3]
            return (Y,m,d)
        def validate(widget):
            text = widget.get_text()
            try:
                parse_date(text)
                error = False
            except ValueError:
                error = True
            self.set_error_status(widget,error)
        def complete(widget, event):
            text = widget.get_text()
            try:
                (Y,m,d) = parse_date(text)
                text = "%02d/%02d/%d" % (d,m,Y)
                widget.set_text(text)
                error = False
            except ValueError:
                error = True
            self.set_error_status(widget,error)
        widget.connect("changed", validate)
        widget.connect("focus-out-event", complete)

    def prefix_Date(self, widget):
        self.prefix_date(widget)
        self.add_mandatory(widget)

    def prefix_age(self, widget):
        def validate(widget):
            text = widget.get_text()
            try:
                age = int(text)
                if age < 16 or age > 99:
                    raise ValueError
                error = False
            except ValueError:
                error = True
            self.set_error_status(widget,error)
        widget.connect("changed", validate)

    def prefix_Age(self, widget):
        self.prefix_age(widget)
        self.add_mandatory(widget)
    
    def prefix_int(self, widget):
        def validate(widget):
            text = widget.get_text()
            try:
                age = int(text)
                error = False
            except ValueError:
                error = True
            self.set_error_status(widget,error)
        widget.connect("changed", validate)

    def prefix_Int(self, widget):
        self.prefix_int(widget)
        self.add_mandatory(widget)

    def prefix_cash(self, widget):
        def validate(widget):
            text = widget.get_text()
            try:
                cash = float(text)
                if cash < 0:
                    raise ValueError
                error = False
            except ValueError:
                error = True
            self.set_error_status(widget,error)
        self.add_mandatory(widget)
        widget.connect("changed", validate)

    def prefix_Cash(self, widget):
        self.prefix_cash(widget)
        self.add_mandatory(widget)

    def add_mandatory(self, widget):
        self.mandatories.append(widget)
        label_prefix = '<b><span color="red">*</span></b>'
        eid = widget.get_name()[3: ]
        label = getattr(self,"lbl%s" % eid)
        markup = label_prefix + label.get_label()
        label.set_markup(markup)

    def set_error_status(self, widget, error_status):
        if error_status:
            color_s = "#FF6B6B"
            widget.set_data("is-valid", None)
        else:
            widget.set_data("is-valid", True)
            color_s = "#FFFFFF"
        color = gtk.gdk.color_parse(color_s)
        widget.modify_base(gtk.STATE_NORMAL, color)
        can_apply = True
        for mandatory in self.mandatories:
            if not mandatory.get_data("is-valid"):
                can_apply = False
        self.btnOk.set_sensitive(can_apply)

class GladeConnect(SimpleGladeApp, PrefixActions):

    def __init__(self, filename=None, root=None):
        SimpleGladeApp.__init__(self, filename, root)
        PrefixActions.__init__(self)
        self.add_prefix_actions(self)
        try:
            self.btnOk.set_sensitive(False)
        except:
            pass
        self.clear_form()

    def on_exit(self, btn=None, data=None):
        self.quit()

    def clear_form(self):
        t = type(gtk.Entry())
        s = type(gtk.SpinButton())
        for i in self.get_widgets():
            if type(i) == t:
                i.set_text("")
            elif type(i) == s:
                min, max = i.get_range()
                i.set_value(min)